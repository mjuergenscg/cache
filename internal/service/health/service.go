package health

import (
	"context"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/cache/gen/health"
)

type Service struct {
	version string
}

func New(version string) *Service {
	return &Service{version: version}
}

func (s *Service) Liveness(_ context.Context) (*health.HealthResponse, error) {
	return &health.HealthResponse{
		Service: "cache",
		Status:  "up",
		Version: s.version,
	}, nil
}

func (s *Service) Readiness(_ context.Context) (*health.HealthResponse, error) {
	return &health.HealthResponse{
		Service: "cache",
		Status:  "up",
		Version: s.version,
	}, nil
}
