package cache_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"

	goacache "gitlab.eclipse.org/eclipse/xfsc/tsa/cache/gen/cache"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/cache/internal/service/cache"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/cache/internal/service/cache/cachefakes"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/errors"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/ptr"
)

func TestNew(t *testing.T) {
	svc := cache.New(nil, nil, zap.NewNop())
	assert.Implements(t, (*goacache.Service)(nil), svc)
}

func TestService_Get(t *testing.T) {
	tests := []struct {
		name  string
		cache *cachefakes.FakeCache
		req   *goacache.CacheGetRequest

		res     interface{}
		errkind errors.Kind
		errtext string
	}{
		{
			name:    "missing cache key",
			req:     &goacache.CacheGetRequest{},
			errkind: errors.BadRequest,
			errtext: "missing key",
		},
		{
			name: "error getting value from cache",
			req: &goacache.CacheGetRequest{
				Key:       "key",
				Namespace: ptr.String("namespace"),
				Scope:     ptr.String("scope"),
			},
			cache: &cachefakes.FakeCache{
				GetStub: func(ctx context.Context, key string) ([]byte, error) {
					return nil, errors.New("some error")
				},
			},
			errkind: errors.Unknown,
			errtext: "some error",
		},
		{
			name: "key not found in cache",
			req: &goacache.CacheGetRequest{
				Key:       "key",
				Namespace: ptr.String("namespace"),
				Scope:     ptr.String("scope"),
			},
			cache: &cachefakes.FakeCache{
				GetStub: func(ctx context.Context, key string) ([]byte, error) {
					return nil, errors.New(errors.NotFound)
				},
			},
			errkind: errors.NotFound,
			errtext: "key not found in cache",
		},
		{
			name: "value returned from cache is not json",
			req: &goacache.CacheGetRequest{
				Key:       "key",
				Namespace: ptr.String("namespace"),
				Scope:     ptr.String("scope"),
			},
			cache: &cachefakes.FakeCache{
				GetStub: func(ctx context.Context, key string) ([]byte, error) {
					return []byte("boom"), nil
				},
			},
			errkind: errors.Unknown,
			errtext: "cannot decode json value from cache",
		},
		{
			name: "json value is successfully returned from cache",
			req: &goacache.CacheGetRequest{
				Key:       "key",
				Namespace: ptr.String("namespace"),
				Scope:     ptr.String("scope"),
			},
			cache: &cachefakes.FakeCache{
				GetStub: func(ctx context.Context, key string) ([]byte, error) {
					return []byte(`{"test":"value"}`), nil
				},
			},
			res:     map[string]interface{}{"test": "value"},
			errtext: "",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			svc := cache.New(test.cache, nil, zap.NewNop())
			res, err := svc.Get(context.Background(), test.req)
			if err == nil {
				assert.Empty(t, test.errtext)
				assert.Equal(t, test.res, res)
			} else {
				assert.Nil(t, res)
				assert.Error(t, err)

				e, ok := err.(*errors.Error)
				assert.True(t, ok)
				assert.Equal(t, test.errkind, e.Kind)
				assert.Contains(t, e.Error(), test.errtext)
			}
		})
	}
}

func TestService_Set(t *testing.T) {
	tests := []struct {
		name  string
		cache *cachefakes.FakeCache
		req   *goacache.CacheSetRequest

		res     interface{}
		errkind errors.Kind
		errtext string
	}{
		{
			name:    "missing cache key",
			req:     &goacache.CacheSetRequest{},
			errkind: errors.BadRequest,
			errtext: "missing key",
		},
		{
			name: "error setting value in cache",
			req: &goacache.CacheSetRequest{
				Key:       "key",
				Namespace: ptr.String("namespace"),
				Scope:     ptr.String("scope"),
				Data:      map[string]interface{}{"test": "value"},
			},
			cache: &cachefakes.FakeCache{
				SetStub: func(ctx context.Context, key string, value []byte, ttl time.Duration) error {
					return errors.New(errors.Timeout, "some error")
				},
			},
			errkind: errors.Timeout,
			errtext: "some error",
		},
		{
			name: "successfully set value in cache",
			req: &goacache.CacheSetRequest{
				Key:       "key",
				Namespace: ptr.String("namespace"),
				Scope:     ptr.String("scope"),
				Data:      map[string]interface{}{"test": "value"},
			},
			cache: &cachefakes.FakeCache{
				SetStub: func(ctx context.Context, key string, value []byte, ttl time.Duration) error {
					return nil
				},
			},
			errtext: "",
		},
		{
			name: "successfully set value in cache with TTL provided in request",
			req: &goacache.CacheSetRequest{
				Key:       "key",
				Namespace: ptr.String("namespace"),
				Scope:     ptr.String("scope"),
				Data:      map[string]interface{}{"test": "value"},
				TTL:       ptr.Int(60),
			},
			cache: &cachefakes.FakeCache{
				SetStub: func(ctx context.Context, key string, value []byte, ttl time.Duration) error {
					return nil
				},
			},
			errtext: "",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			svc := cache.New(test.cache, nil, zap.NewNop())
			err := svc.Set(context.Background(), test.req)
			if err == nil {
				assert.Empty(t, test.errtext)
			} else {
				assert.Error(t, err)
				e, ok := err.(*errors.Error)
				assert.True(t, ok)
				assert.Equal(t, test.errkind, e.Kind)
				assert.Contains(t, e.Error(), test.errtext)
			}
		})
	}
}

func TestService_SetExternal(t *testing.T) {
	tests := []struct {
		name   string
		cache  *cachefakes.FakeCache
		events *cachefakes.FakeEvents
		req    *goacache.CacheSetRequest

		res     interface{}
		errkind errors.Kind
		errtext string
	}{
		{
			name: "error setting external input in cache",
			req: &goacache.CacheSetRequest{
				Key:       "key",
				Namespace: ptr.String("namespace"),
				Scope:     ptr.String("scope"),
				Data:      map[string]interface{}{"test": "value"},
			},
			cache: &cachefakes.FakeCache{
				SetStub: func(ctx context.Context, key string, value []byte, ttl time.Duration) error {
					return errors.New(errors.Timeout, "some error")
				},
			},
			errkind: errors.Timeout,
			errtext: "some error",
		},
		{
			name: "error sending an event for external input",
			req: &goacache.CacheSetRequest{
				Key:       "key",
				Namespace: ptr.String("namespace"),
				Scope:     ptr.String("scope"),
				Data:      map[string]interface{}{"test": "value"},
			},
			cache: &cachefakes.FakeCache{
				SetStub: func(ctx context.Context, key string, value []byte, ttl time.Duration) error {
					return nil
				},
			},
			events: &cachefakes.FakeEvents{SendStub: func(ctx context.Context, s string) error {
				return errors.New(errors.Unknown, "failed to send event")
			}},
			errkind: errors.Unknown,
			errtext: "failed to send event",
		},
		{
			name: "successfully set value in cache and send an event to events",
			req: &goacache.CacheSetRequest{
				Key:       "key",
				Namespace: ptr.String("namespace"),
				Scope:     ptr.String("scope"),
				Data:      map[string]interface{}{"test": "value"},
			},
			cache: &cachefakes.FakeCache{
				SetStub: func(ctx context.Context, key string, value []byte, ttl time.Duration) error {
					return nil
				},
			},
			events: &cachefakes.FakeEvents{SendStub: func(ctx context.Context, s string) error {
				return nil
			}},
			errtext: "",
		},
		{
			name: "successfully set value in cache with TTL provided in request and send an event to events",
			req: &goacache.CacheSetRequest{
				Key:       "key",
				Namespace: ptr.String("namespace"),
				Scope:     ptr.String("scope"),
				Data:      map[string]interface{}{"test": "value"},
				TTL:       ptr.Int(60),
			},
			cache: &cachefakes.FakeCache{
				SetStub: func(ctx context.Context, key string, value []byte, ttl time.Duration) error {
					return nil
				},
			},
			events: &cachefakes.FakeEvents{SendStub: func(ctx context.Context, s string) error {
				return nil
			}},
			errtext: "",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			svc := cache.New(test.cache, test.events, zap.NewNop())
			err := svc.SetExternal(context.Background(), test.req)
			if err == nil {
				assert.Empty(t, test.errtext)
			} else {
				assert.Error(t, err)
				e, ok := err.(*errors.Error)
				assert.True(t, ok)
				assert.Equal(t, test.errkind, e.Kind)
				assert.Contains(t, e.Error(), test.errtext)
			}
		})
	}
}
