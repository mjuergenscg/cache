package cache

import (
	"context"
	"encoding/json"
	"time"

	"go.uber.org/zap"

	"gitlab.eclipse.org/eclipse/xfsc/tsa/cache/gen/cache"
	"gitlab.eclipse.org/eclipse/xfsc/tsa/golib/errors"
)

//go:generate counterfeiter . Cache
//go:generate counterfeiter . Events

type Cache interface {
	Get(ctx context.Context, key string) ([]byte, error)
	Set(ctx context.Context, key string, value []byte, ttl time.Duration) error
}

type Events interface {
	Send(ctx context.Context, key string) error
}

type Service struct {
	cache  Cache
	events Events
	logger *zap.Logger
}

func New(cache Cache, events Events, logger *zap.Logger) *Service {
	return &Service{
		cache:  cache,
		events: events,
		logger: logger,
	}
}

func (s *Service) Get(ctx context.Context, req *cache.CacheGetRequest) (interface{}, error) {
	logger := s.logger.With(zap.String("operation", "get"))

	if req.Key == "" {
		logger.Error("bad request: missing key")
		return nil, errors.New(errors.BadRequest, "missing key")
	}

	// create key from the input fields
	key := makeCacheKey(req.Key, req.Namespace, req.Scope)
	data, err := s.cache.Get(ctx, key)
	if err != nil {
		logger.Error("error getting value from cache", zap.String("key", key), zap.Error(err))
		if errors.Is(errors.NotFound, err) {
			return nil, errors.New("key not found in cache", err)
		}
		return nil, errors.New("error getting value from cache", err)
	}

	var decodedValue interface{}
	if err := json.Unmarshal(data, &decodedValue); err != nil {
		logger.Error("cannot decode json value from cache", zap.Error(err))
		return nil, errors.New("cannot decode json value from cache", err)
	}

	return decodedValue, nil
}

func (s *Service) Set(ctx context.Context, req *cache.CacheSetRequest) error {
	logger := s.logger.With(zap.String("operation", "set"))

	if req.Key == "" {
		logger.Error("bad request: missing key")
		return errors.New(errors.BadRequest, "missing key")
	}

	// create key from the input fields
	key := makeCacheKey(req.Key, req.Namespace, req.Scope)
	// encode payload to json bytes for storing in cache
	value, err := json.Marshal(req.Data)
	if err != nil {
		logger.Error("error encode payload to json", zap.Error(err))
		return errors.New(errors.BadRequest, "cannot encode payload to json", err)
	}

	// set cache ttl if provided in request
	var ttl time.Duration
	if req.TTL != nil {
		ttl = time.Duration(*req.TTL) * time.Second
	}

	if err := s.cache.Set(ctx, key, value, ttl); err != nil {
		logger.Error("error storing value in cache", zap.Error(err))
		return errors.New("error storing value in cache", err)
	}

	return nil
}

// SetExternal sets an external JSON value in the cache and provide an event for the input.
func (s *Service) SetExternal(ctx context.Context, req *cache.CacheSetRequest) error {
	logger := s.logger.With(zap.String("operation", "setExternal"))

	// set value in cache
	if err := s.Set(ctx, req); err != nil {
		logger.Error("error setting external input in cache", zap.Error(err))
		return errors.New("error setting external input in cache", err)
	}

	// create key from the input fields
	key := makeCacheKey(req.Key, req.Namespace, req.Scope)

	// send an event for the input
	if err := s.events.Send(ctx, key); err != nil {
		logger.Error("error sending an event for external input", zap.Error(err))
		return errors.New("error sending an event for external input", err)
	}

	return nil
}

func makeCacheKey(key string, namespace, scope *string) string {
	k := key
	if namespace != nil && *namespace != "" {
		k += "," + *namespace
	}
	if scope != nil && *scope != "" {
		k += "," + *scope
	}
	return k
}
